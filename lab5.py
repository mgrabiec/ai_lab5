from spacy.matcher import Matcher
from spacy import displacy
import spacy
from spacy.tokenizer import Tokenizer
import re
from collections import Counter

nlp = spacy.load("pl_core_news_sm")

introduction_text = (
    'This tutorial is about Natural' ' Language Processing in Spacy.')
introduction_doc = nlp(introduction_text)
print([token.text for token in introduction_doc])

file_name = 'test.txt'
introduction_file_text = open(file_name).read()
introduction_file_doc = nlp(introduction_file_text)
print([token.text for token in introduction_file_doc])

about_text = ('Już ledwo sapie, już ledwo zipie,'
                'A jeszcze palacz węgiel w nią sypie.'
                'Wagony do niej podoczepiali'
                'Wielkie i ciężkie, z żelaza, stali,'
                'I pełno ludzi w każdym wagonie,')
about_doc = nlp(about_text)
sentences = list(about_doc.sents)
len(sentences)
for sentence in sentences:
    print(sentence)

def set_custom_boundaries(doc):
    for token in doc[:-1]:
        if token.text == ',':
            doc[token.i+1].is_sent_start = True
    return doc


ellipsis_text = ("""Wczoraj byłem gdzieś na końcu świata,
Głaszcząc słonie na lankijskich wioskach,
Nie wiesz co się robi w rezerwatach,
Nie wiesz co potrafi zrobić forsa,
Wczoraj byłem szejkiem w Emiratach,
I krążyłem po paryskim getcie,
Przetańczyłem długą noc na Lapa,
Brazylijska kawa ma jebnięcie,
Zajadałem pizzę w L'Archette,
Popijałem sake w Nanashi,
Podawali kolumbijski tester,
Ale weź mi wypierdalaj z tym,
Wyruszyłem w kraj na stopa chłopak,
Ten najdzikszy który znałeś z bajek""")
custom_nlp = spacy.load('pl_core_news_sm')
custom_nlp.add_pipe(set_custom_boundaries, before='parser')
custom_ellipsis_doc = custom_nlp(ellipsis_text)
custom_ellipsis_sentences = list(custom_ellipsis_doc.sents)
for sentence in custom_ellipsis_sentences:
    print(sentence)


custom_nlp = spacy.load('pl_core_news_sm')
ellipsis_doc = nlp(ellipsis_text)
ellipsis_sentences = list(ellipsis_doc.sents)
print('sentence Detection with no customization')
for sentence in ellipsis_sentences:
    print('\n')
    print(sentence)


for token in about_doc:
    print(token, token.idx)

for token in about_doc:
    print(token, token.idx, token.text_with_ws,
          token.is_alpha, token.is_punct, token.is_space, token.shape_, token.is_stop)

custom_nlp = spacy.load('pl_core_news_sm')
prefix_re = spacy.util.compile_prefix_regex(custom_nlp.Defaults.prefixes)
suffix_re = spacy.util.compile_suffix_regex(custom_nlp.Defaults.suffixes)
infix_re = re.compile(r'''[-~]''')


def customize_tokenizer(nlp):
    return Tokenizer(nlp.vocab, prefix_search=prefix_re.search, suffix_search=suffix_re.search,
                     infix_finditer=infix_re.finditer, token_match=None)


custom_nlp.tokenizer = customize_tokenizer(custom_nlp)
custom_tokenizer_about_doc = custom_nlp(about_text)
print([token.text for token in custom_tokenizer_about_doc])

spacy_stopwords = spacy.lang.pl.stop_words.STOP_WORDS
print(len(spacy_stopwords))
for stop_word in list(spacy_stopwords)[:20]:
    print(stop_word)

for token in about_doc:
    if not token.is_stop:
        print(token)

about_no_stopword_doc = [token for token in about_doc if not token.is_stop]
print(about_no_stopword_doc)

conference_help_text = ("""Pradawna prawda jest tu dziś nieznana
Jak mantra, zadziałało kłamstwo szarlatana
Jestem sceptyczny patrząc na ten dramat
I nie wierzę w ani jedno hasło na ich barykadach
Oni tak pewni swego, wczoraj na bogato
Dzisiaj błagalnie patrzą na mnie, co ja na to
Nabrał ich marnie, jakiś prestidigitator
Poszli triumfalnie pod nóż, jak baranów stado
Żeby nie widzieli prawdy, dostali zajęcie
Materialne życie, co ucieka co raz prędzej
Słuchali tych, co im obiecali więcej,
że pięknie będzie, że wszystko na patencie
Licz, wydaj, konsumuj i używaj jeszcze""")
conference_help_doc = nlp(conference_help_text)
for token in conference_help_doc:
    print(token, token.lemma_)


complete_text = ("""
Pradawna prawda jest tu dziś nieznana
Jak mantra, zadziałało kłamstwo szarlatana
Jestem sceptyczny patrząc na ten dramat
I nie wierzę w ani jedno hasło na ich barykadach
Oni tak pewni swego, wczoraj na bogato
Dzisiaj błagalnie patrzą na mnie, co ja na to
Nabrał ich marnie, jakiś prestidigitator
Poszli triumfalnie pod nóż, jak baranów stado
Żeby nie widzieli prawdy, dostali zajęcie
Materialne życie, co ucieka co raz prędzej
Słuchali tych, co im obiecali więcej,
że pięknie będzie, że wszystko na patencie
Licz, wydaj, konsumuj i używaj jeszcze""")

complete_doc = nlp(complete_text)

words = [token.text for token in complete_doc
         if not token.is_stop and not token.is_punct]
print('\nCount words:')
word_freq = Counter(words)

common_words = word_freq.most_common(5)
print('\nCommon words')
print(common_words)

unique_words = [word for (word, freq) in word_freq.items() if freq == 1]
print('\n Unique words')
print(unique_words)

words_all = [token.text for token in complete_doc if not token.is_punct]
print('\n Counter words all')
word_freq_all = Counter(words_all)

print('\n Common words all')
common_words_all = word_freq_all.most_common(5)
print(common_words_all)


for token in about_doc:
    print(token, token.tag_, token.pos_, spacy.explain(token.tag_))


nouns = []
adjectives = []
for token in about_doc:
    if token.pos_ == 'NOUN':
        nouns.append(token)
    if token.pos_ == 'ADJ':
        adjectives.append(token)


def is_token_allowed(token):
    if (not token or not token.string.strip() or
            token.is_stop or token.is_punct):
        return False
    return True


def preprocess_token(token):
    return token.lemma_.strip().lower()

complete_filtered_tokens = [preprocess_token(token)
                            for token in complete_doc if is_token_allowed(token)]
print(complete_filtered_tokens)

matcher = Matcher(nlp.vocab)
conference_org_text = ("""Lorem ipsum dolor sit amet,
consectetur adipiscing elit,
sed do eiusmod tempor incididunt
ut labore et dolore magna aliqua. 365-213-333""")


def extract_phone_number(nlp_doc):
    pattern = [{'SHAPE': 'ddd'}, {'ORTH': '-', 'OP': '?'}, {'SHAPE': 'ddd'}, {'ORTH': '-', 'OP': '?'},
               {'SHAPE': 'ddd'}]
    matcher.add('PHONE_NUMBER', None, pattern)
    matches = matcher(nlp_doc)
    for match_id, start, end in matches:
        span = nlp_doc[start:end]
        return span.text


conference_org_doc = nlp(conference_org_text)
print("\n\n\n\nPhone number:")
print(extract_phone_number(conference_org_doc))

print('\n\n\n')
dummy_text = 'Adam codziennie zaskakiwał Ewę, ale ta nie lubiła niespodzianek.'
dummy_doc = nlp(dummy_text)
for token in dummy_doc:
    print(token.text, token.tag_, token.head.text, token.dep_)




one_line_about_text = ('Adam codziennie zaskakiwał Ewę, ale ta nie lubiła niespodzianek.')
one_line_about_doc = nlp(one_line_about_text)

print('\n\n\n')
print(list(one_line_about_doc[5].subtree))

def flatten_tree(tree):
    return ''.join([token.text_with_ws for token in list(tree)]).strip()

print (flatten_tree(one_line_about_doc[5].subtree))

displacy.serve(dummy_doc, style='dep')